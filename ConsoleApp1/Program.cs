﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var response = GetProfile();
        }


        public static string GetProfile()
        {
            // set your own keys and screen name
            var oauth_consumer_key = "test_3wvfna5beds7w9vqlizm1j7vprvsrz";
            var oauth_consumer_secret = "1vgkt6mhita7vvp8hjvtrkyqikxcxdxeotaruum29kbb8splxe";
            var oAuthUrl = "https://api.cardinity.com/v1/payments";
            // Authenticate

            //var oauthNonce = Convert.ToBase64String(
            //    new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));

            var oauthNonce = GetNonce();

            var timeSpan = DateTime.UtcNow
                           - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

            var oauthTimestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // create oauth signature
            var baseFormat = "oauth_consumer_key={0}&oauth_signature_method={1}&oauth_timestamp={2}" +
                         "&oauth_nonce={3}&oauth_version={4}";

            var baseString = string.Format(baseFormat,
                oauth_consumer_key,
                oauth_signature_method,
                oauthTimestamp,
                oauthNonce,
                oauth_version
            );

            baseString = string.Concat("POST&", Uri.EscapeDataString("https://api.cardinity.com/v1/payments"), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat("POST&", Uri.EscapeDataString(oAuthUrl), "&", Uri.EscapeDataString(oauth_consumer_key), "&", Uri.EscapeDataString(oauth_signature_method),
                "&", Uri.EscapeDataString(oauthTimestamp), "&", Uri.EscapeDataString(oauthNonce),
                "&", Uri.EscapeDataString(oauth_version));

            //var compositeKey = string.Concat("POST&", Uri.EscapeDataString(oAuthUrl), "&", Uri.EscapeDataString(oauth_consumer_key), "&", Uri.EscapeDataString(oauth_signature_method),
            //    "&", Uri.EscapeDataString(oauthTimestamp), "&", Uri.EscapeDataString(oauthNonce),
            //    "&", Uri.EscapeDataString(oauth_version), "&", Uri.EscapeDataString(oauth_consumer_secret));

            string oauthSignature;

            using (HMACSHA1 hasher = new HMACSHA1(Encoding.ASCII.GetBytes(compositeKey)))
            {
                oauthSignature = Convert.ToBase64String(
                    hasher.ComputeHash(Encoding.ASCII.GetBytes(baseString)));
            }

            // create the request header

            var headerFormat = "OAuth oauth_consumer_key=\"{0}\", oauth_signature_method=\"{1}\", " +
                               "oauth_timestamp=\"{2}\", oauth_nonce=\"{3}\", " +
                               "oauth_version=\"{4}\", oauth_signature=\"{5}\" ";

            var authHeader = string.Format(headerFormat,
                Uri.EscapeDataString(oauth_consumer_key),
                Uri.EscapeDataString(oauth_signature_method),
                Uri.EscapeDataString(oauthTimestamp),
                Uri.EscapeDataString(oauthNonce),
                Uri.EscapeDataString(oauth_version),
                Uri.EscapeDataString(oauthSignature)
            );

            HttpWebRequest authRequest = (HttpWebRequest)WebRequest.Create(oAuthUrl);
            authRequest.Headers.Add("Authorization", authHeader);
            authRequest.Method = "POST";
            authRequest.ContentType = "application/json;";
            authRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            WebResponse authResponse = authRequest.GetResponse();
            // deserialize into an object
            using (authResponse)
            {
                using (var reader = new StreamReader(authResponse.GetResponseStream() ?? throw new InvalidOperationException()))
                {
                    String js = reader.ReadToEnd();
                    return js;
                }
            }
        }

        private static string GetNonce()
        {
            string rtn = Path.GetRandomFileName() + Path.GetRandomFileName() + Path.GetRandomFileName() + Path.GetRandomFileName();
            rtn = rtn.Replace(".", "");

            if (rtn.Length > 32) return rtn.Substring(0, 32);
            return rtn;
        }
    }
}
